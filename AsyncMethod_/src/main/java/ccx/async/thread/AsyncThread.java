package ccx.async.thread;

import java.util.concurrent.TimeUnit;

public class AsyncThread implements Runnable{
    @Override
    public void run() {
        System.out.println("异步线程开始");
        long start = System.currentTimeMillis();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        long end = System.currentTimeMillis();
        System.out.println("异步线程：" + Thread.currentThread().getName() + "结束，耗时：" + (end - start));
    }
}

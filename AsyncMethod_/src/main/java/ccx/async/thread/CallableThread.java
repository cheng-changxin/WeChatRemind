package ccx.async.thread;

import org.springframework.util.StopWatch;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class CallableThread implements Callable {
    @Override
    public String call() throws Exception {
//        int a = 1/0;
        long start = System.currentTimeMillis();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println("callable任务开始执行:" + start);
        TimeUnit.SECONDS.sleep(2);
        System.out.println();
        stopWatch.stop();
        System.out.println("stopWatch.prettyPrint------");
        System.out.println(stopWatch.prettyPrint());
        System.out.println("stopWatch.shortSummary------");
        System.out.println(stopWatch.shortSummary());
        System.out.println("stopWatch.getTotalTimeMillis------");
        System.out.println(stopWatch.getTotalTimeMillis());
        return "call执行结束 ";
    }
}

package ccx.async.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置，可以配置多个线程池
 * @Async注解，默认使用系统自定义线程池，可在项目中设置多个线程池，在异步调用的时候，指明需要调用的线程池名称
 * 比如：@Async("线程池1")
 */
@Configuration
public class ExecutorConfig {
    /**
     * 自定义线程池
     */
    @Bean("myExecutor")
    public Executor taskExecutor(){
        System.out.println("系统最大线程数:" + Runtime.getRuntime().availableProcessors());
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(8);//核心线程数
        threadPoolTaskExecutor.setMaxPoolSize(16);//最大线程数
        threadPoolTaskExecutor.setQueueCapacity(1000);//配置队列容量
        threadPoolTaskExecutor.setKeepAliveSeconds(60);//空闲线程存活时间
        threadPoolTaskExecutor.setThreadNamePrefix("myExecutor-");//线程名字前缀
        return threadPoolTaskExecutor;
    }
}

package ccx.async.handler;

import ccx.async.event.LoginEvent;
import ccx.async.event.MessageEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

@Component
public class EventPublisher implements ApplicationEventPublisherAware {
    private ApplicationEventPublisher publisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    public void publish(ApplicationEvent event){
        if (event instanceof MessageEvent){
            System.out.println("开始发布MessageEvent事件:" + ((MessageEvent) event).getMessage());
        } else if (event instanceof LoginEvent) {
            System.out.println("开始发布LoginEvent事件:" + ((LoginEvent) event).getUsername());
        }
        publisher.publishEvent(event);
        System.out.println("事件发布结束");
    }

}

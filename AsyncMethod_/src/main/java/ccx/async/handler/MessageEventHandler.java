package ccx.async.handler;

import ccx.async.event.LoginEvent;
import ccx.async.event.MessageEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class MessageEventHandler {

    @Async
    @EventListener
    public void handleLoginEvent(LoginEvent event){
        System.out.println("接受到LoginEvent事件");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(event.getUsername());
        System.out.println("LoginEvent事件处理完成");
    }

    @Async
    @EventListener
    public void handleMessageEvent(MessageEvent event){
        System.out.println("接受到MessageEvent事件");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(event.getMessage());
        System.out.println("MessageEvent事件处理完成");
    }

}

package ccx.async.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

public class MessageEvent extends ApplicationEvent {
    @Getter
    private String message;

    public MessageEvent(Object source, String message) {
        super(source);
        this.message = message;
    }
}

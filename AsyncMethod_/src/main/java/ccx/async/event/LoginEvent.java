package ccx.async.event;

import lombok.Data;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.Date;

public class LoginEvent extends ApplicationEvent {
    @Getter
    private String username;

    private Date loginDate;

    public LoginEvent(Object source,String username, Date loginDate) {
        super(source);
        this.username = username;
        this.loginDate = loginDate;
    }
}

package ccx.async.controller;

import ccx.async.event.LoginEvent;
import ccx.async.event.MessageEvent;
import ccx.async.handler.EventPublisher;
import ccx.async.service.AsyncService;
import ccx.async.thread.AsyncThread;
import ccx.async.thread.CallableThread;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.*;

@RestController
@RequestMapping("/async")
public class AsyncController {

    @Resource
    private AsyncService asyncService;

    @Resource
    private EventPublisher eventPublisher;

    @GetMapping("/thread")
    public String asyncThread(){
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(3, 3, 5, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10), new ThreadPoolExecutor.AbortPolicy());
        long start = System.currentTimeMillis();
        //自己的业务代码。。。
        AsyncThread asyncThread = new AsyncThread();
        threadPool.execute(asyncThread);
        long end = System.currentTimeMillis();
        return "返回,耗时:" + (end - start);
    }

    @GetMapping("/threadFuture")
    public String threadFuture(){
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(3, 3, 5, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10), new ThreadPoolExecutor.AbortPolicy());
        long start = System.currentTimeMillis();
        CallableThread callableThread = new CallableThread();
        Future<String> submit = threadPool.submit(callableThread);
        try {
            //在获取返回值时会阻塞主线程
            String s = "";
            s = submit.get();
            System.out.println(s);
        } catch (Exception e) {
            System.out.println("线程运行发生错误" + e.getMessage());
            throw new RuntimeException(e);
        }
        long end = System.currentTimeMillis();
        return "接口返回,耗时:" + (end - start);
    }

    @GetMapping("/asyncMethod")
    public String asyncMethod(){
        System.out.println("aaa");
        System.out.println("调用异步方法");
        asyncService.sendMsg();
        System.out.println("bbb");
        return "asyncMethod方法返回";
    }

    /**
     * 同一个类中调用标注了Async注解的方法是无法异步的，仍然是同步
     * 因为异步是通过AOP实现的，调用标注了@Async的方法时调用的是代理对象，然后调用invoke执行方法，将方法提交给线程池中其它线程去执行
     * 所以如果a方法调用它同类中的标注@Async的b方法，是不会异步执行的，因为从a方法进入调用的都是该类对象本身，不会进入代理类
     * @return
     */
    @Async
    public String async1(){
        System.out.println("进入异步方法1");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("异步方法1执行完成");
        return "异步方法1执行完成";
    }

    @GetMapping("/asyncFutureMethod")
    public String asyncFutureMethod(){
        System.out.println("aaa");
        Future<String> stringFuture = asyncService.sendMsgFuture();
        System.out.println("bbb");
        try {
            System.out.println(stringFuture.get());//get方法会阻塞主线程
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return "asyncfutureMethod方法返回";
    }

    @GetMapping("/pubEvent")
    public String publishEvent(){
        System.out.println("业务逻辑开始");
        eventPublisher.publish(new MessageEvent(this,"testEvent"));
        //发布登陆成功事件
//        eventPublisher.publish(new LoginEvent(this,"ccx",new Date()));
        System.out.println("业务逻辑结束");
        return "发布成功";
    }

}

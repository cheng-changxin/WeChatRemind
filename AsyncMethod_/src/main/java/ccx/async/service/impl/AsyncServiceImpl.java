package ccx.async.service.impl;

import ccx.async.service.AsyncService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Service
public class AsyncServiceImpl implements AsyncService {
    @Override
    @Async("myExecutor")
    public void sendMsg() {
        System.out.println("进入异步方法");
        System.out.println("当前线程名称：" + Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("异步方法执行完成");
    }

    /**
     * 具有返回值的异步方法，返回类型为Future,返回时new 一个AsyncResult对象，其中参数为返回的内容
     * @return
     */
    @Override
    @Async("myExecutor")
    public Future<String> sendMsgFuture() {
        System.out.println("进入future异步方法");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return new AsyncResult<>("future异步方法执行完成");
    }
}

package ccx.async.service;

import java.util.concurrent.Future;

public interface AsyncService {
    void sendMsg();

    Future<String> sendMsgFuture();
}

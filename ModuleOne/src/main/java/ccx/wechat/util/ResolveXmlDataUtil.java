package ccx.wechat.util;

import ccx.wechat.domain.WxUserMessageModel;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.AnyTypePermission;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class ResolveXmlDataUtil {
    /**
     * 解析xml数据
     *
     * @param in 输入流
     * @return 微信用户信息实体类
     * @throws IOException
     */
    public static WxUserMessageModel resolveXmlData(InputStream in) throws IOException {
        String xmlData = IOUtils.toString(in);
        WxUserMessageModel wxXmlData = null;
        XStream xstream = new XStream();
        //		尽量限制所需的最低权限 这条语句解决该问题 com.thoughtworks.xstream.security.ForbiddenClassException
        xstream.addPermission(AnyTypePermission.ANY);
        // 设置加载类的类加载器
        xstream.setClassLoader(WxUserMessageModel.class.getClassLoader());
        xstream.processAnnotations(WxUserMessageModel.class);
        xstream.alias("xml", WxUserMessageModel.class);
        wxXmlData = (WxUserMessageModel) xstream.fromXML(xmlData);
        return wxXmlData;
    }

}

package ccx.wechat.controller;

import ccx.wechat.config.ScheduleConfig;
import ccx.wechat.config.WXConfig;
import ccx.wechat.domain.WXMessgeBean;
import ccx.wechat.domain.WxUserMessageModel;
import ccx.wechat.schedule.DynamicTask;
import ccx.wechat.util.CheckoutUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.google.gson.JsonObject;
import com.thoughtworks.xstream.XStream;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpTemplateMsgService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpMapConfigImpl;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static ccx.wechat.util.ResolveXmlDataUtil.resolveXmlData;
import static cn.hutool.core.lang.Validator.validateNull;

@Slf4j
@Controller
@RequestMapping(value = "/wx")
public class WeiController {
    @Value("${wx.appid}")
    private String appid;

    @Value("${wx.secret}")
    private String secret;

    @Autowired
    private WXConfig wxConfig;

    @Autowired
    DynamicTask dynamicTask;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * token验证
     * @param request
     * @param response
     */
    @GetMapping(value = "/weChatToken")
    public void wechat(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //token验证
            // 微信加密签名
        String signature = request.getParameter("signature");
            // 时间戳
        String timestamp = request.getParameter("timestamp");
            // 随机数
        String nonce = request.getParameter("nonce");
            // 随机字符串
        String echostr = request.getParameter("echostr");
            // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
        if (signature != null && CheckoutUtil.checkSignature(signature,timestamp,nonce)){
            PrintWriter writer = response.getWriter();
            writer.write(echostr);
            writer.flush();
        }
    }

    /**
     * 微信消息接收，本来想用于用户点击已打卡按钮然后回复用户一条消息，但是返回消息一直不到微信，解决不了，放弃
     * 所以就不返回消息了，用户发送1然后保存上他的状态，后边不再给你发送模板消息提醒他打卡就行了
     */
    @PostMapping(value = "/weChatToken", produces = MediaType.APPLICATION_XML_VALUE)
    public String wechatMsgReceive(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("接收到用户的消息:{}",request.getInputStream());
        // 数据提取
        WxUserMessageModel wxUserMessageModel = resolveXmlData(request.getInputStream());
        if (Objects.isNull(wxUserMessageModel)) {
            // 不回消息也需要返回"success"
            return "success";
        }
        // 返回数据
        WxUserMessageModel responseXmlData = new WxUserMessageModel();
        responseXmlData.setToUserName(wxUserMessageModel.getFromUserName());
        responseXmlData.setFromUserName(wxUserMessageModel.getToUserName());
        responseXmlData.setCreateTime(new Date().getTime());
        responseXmlData.setMsgType("text");
        String openID = wxUserMessageModel.getFromUserName();
        // 按需编写业务
        if (StringUtils.equals(wxUserMessageModel.getContent(), "1")) {
            System.out.println("用户:" + openID + "完成打卡");
            responseXmlData.setContent("好的，今天不再提醒您打卡");
            //将openID存入redis，对于已存入redis的openID不再给它推送打卡消息
//            ValueOperations valueOperations = redisTemplate.opsForValue();
//            valueOperations.set("openID",openID);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String key = "openID"+dateFormat.format(new Date());
            ListOperations listOperations = redisTemplate.opsForList();
            Long aLong = listOperations.leftPush(key, openID);
            System.out.println("用户:" + openID + "完成打卡,存入redis:" + aLong);
            //设置key的过期时间，到12点过期
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR,23);
            calendar.set(Calendar.MINUTE,59);
            long expireTime = calendar.getTimeInMillis() - System.currentTimeMillis();
            redisTemplate.expire(key, expireTime, TimeUnit.MILLISECONDS);
        } /*else if (StringUtils.equals(wxUserMessageModel.getContent(), "xx")) {
            responseXmlData.setContent(checkCheckState(wxUserMessageModel.getFromUserName()));
        }  else {
            responseXmlData.setContent(wxUserMessageModel.getContent());
        }*/

        //如果点击的是“已打卡”按钮
        //用openID存入redis，对于已存入redis的openID不再给它推送打卡消息
        if ("complete".equals(wxUserMessageModel.getEventkey())){
            responseXmlData.setContent("好的，今天不再提醒您打卡");
        }
        XStream xstream = new XStream();
        xstream.processAnnotations(WxUserMessageModel.class);
        xstream.setClassLoader(WxUserMessageModel.class.getClassLoader());
        return xstream.toXML(responseXmlData);  //XStream的方法，直接将对象转换成 xml数据
    }



    /**
     * 开始任务
     * @return
     */
    @GetMapping("/testStarkTask")
    @ResponseBody
    public String testStarkTask(){
        dynamicTask.startTask();
        log.info("定时任务开始");
        return "定时任务开始";
    }

    @GetMapping("/getRunningTask")
    @ResponseBody
    public String getRunningTask(){
        ConcurrentHashMap.KeySetView<String, ScheduledFuture> keySetView = ScheduleConfig.cacheSchedule.keySet();
        String runningTasks = keySetView.stream()
                .collect(Collectors.joining(","));
        return runningTasks;
    }

    /**
     * 停止正在执行的任务
     * @param name
     * @return
     */
    @GetMapping("/testStopTask/{name}")
    @ResponseBody
    public String testStopTask(@PathVariable("name") String name){
        boolean b = dynamicTask.stopTask(name);
        log.info("停止任务{}：{}",name,b);
        return b ? "成功" : "失败";
    }

    /**
     * 给微信公众号所有用户推送信息
     */
    @GetMapping("/send")
    @ResponseBody
    public void pushMessage() throws IOException {
        String access_token = getAccessToken();
        //获取用户列表
        JSONObject userList = getUserList(access_token);
        JSONObject data1 = userList.getJSONObject("data");
        JSONArray openidArray = data1.getJSONArray("openid");
        List<String> openIdList = JSONUtil.toList(openidArray, String.class);

        String requestUrl = " https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="  + access_token;
        HashMap<String, Object> content = new HashMap<>();
        content.put("template_id", wxConfig.getTemplateID());

        HashMap<String, String> data = new HashMap<>();
        data.put("first","姓名");
        content.put("data",data);

        String resp = "";

        Iterator<String> iterator = openIdList.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();
            content.put("touser", next);
            resp = HttpUtil.post(requestUrl,JSONUtil.toJsonPrettyStr(content));
        }
        JSONObject result = JSONUtil.parseObj(resp);
        System.out.println("发送消息:" + result);
//        return result;
    }

    /**
     * 获取accessToken
     * @return
     * @throws IOException
     */
    public String getAccessToken() throws IOException {
//        String accessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&";
//        String requestUrl = accessTokenUrl + "appid=" + appid + "&secret=" + secret;
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(wxConfig.getGetAccessTokenUrl());
        HttpResponse execute = httpClient.execute(httpPost);
        InputStream content = execute.getEntity().getContent();
        OutputStream outputStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int n ;
        while ((n = content.read(bytes)) != -1) {
            outputStream.write(bytes,0,n);
        }
        outputStream.flush();
        outputStream.close();
        String string = outputStream.toString();
        JSONObject jsonObject = JSONUtil.parseObj(string);
        String access_token = jsonObject.getStr("access_token");
        return access_token;
    }

    /**
     * 获取用户列表
     * @param accessToken
     * @return
     */

    public JSONObject getUserList(String accessToken) {
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + accessToken + "&next_openid=";
        String resp = HttpUtil.get(requestUrl);
        JSONObject result = JSONUtil.parseObj(resp);
        System.out.println("用户列表:" + resp);
        return result;
    }


}

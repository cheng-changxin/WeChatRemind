package ccx.wechat.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "wx")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXConfig {
    private String appid;

    private String secret;

    private String templateID;

    private String getAccessTokenUrl;

}

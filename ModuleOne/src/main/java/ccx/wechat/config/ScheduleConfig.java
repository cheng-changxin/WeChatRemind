package ccx.wechat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

/*
 * @Description TODO (配置定时任务线程数)
 * 创建人： 程长新
 * 创建时间：2023/10/20 17:30
 **/
@Configuration
public class ScheduleConfig {
    //用于保存正在执行的定时任务，方便停止对应任务
    public static ConcurrentHashMap<String, ScheduledFuture> cacheSchedule = new ConcurrentHashMap<String, ScheduledFuture>();

    //定义用于执行定时任务的线程数量
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler(){
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(3);
        return threadPoolTaskScheduler;
    }
}

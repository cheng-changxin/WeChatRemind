package ccx.wechat.schedule;

import ccx.wechat.config.WXConfig;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/*
 * @Description TODO (发送提醒任务)
 * 创建人： 程长新
 * 创建时间：2023/10/23 8:40
 **/
@NoArgsConstructor
@AllArgsConstructor
@Data
@Slf4j
public class SendMsgTask implements Runnable{

    private String name;//任务名称
    private String corn;//触发条件

    private String getAccessTokenUrl;
    private String templateID;

    private RedisTemplate redisTemplate;

    SendMsgTask(String name, String corn){
        this.name = name;
        this.corn = corn;
    }

    @Override
    public void run() {
        String access_token = null;
        try {
            access_token = getAccessToken();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //获取用户列表
        JSONObject userList = getUserList(access_token);
        JSONObject data1 = userList.getJSONObject("data");
        JSONArray openidArray = data1.getJSONArray("openid");
        List<String> openIdList = JSONUtil.toList(openidArray, String.class);

        String requestUrl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="  + access_token;
        HashMap<String, Object> content = new HashMap<>();
        content.put("template_id", templateID);

        HashMap<String, String> data = new HashMap<>();
        data.put("first","姓名");
        content.put("data",data);

        String resp = "";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String key = "openID"+dateFormat.format(new Date());
        int count = 0;
        Iterator<String> iterator = openIdList.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();
            //检查用户是否已经完成签到，如果已经完成就不给他发消息
            List<String> range = redisTemplate.opsForList().range(key, 0, -1);
            boolean b = range.stream().anyMatch(openID -> openID.equals(next));
            if (b) {
                System.out.println("用户:" + next + "已完成打卡");
                //记录已经打卡的用户数
                count++;
                continue;
            }
            content.put("touser",next);
            resp = HttpUtil.post(requestUrl,JSONUtil.toJsonPrettyStr(content));
            log.info("给用户{}发送消息,{}",next,resp);
        }
/*      //这段代码不能要了，一开始设置的任务周期是6点，但是是错误的，只在6点的每5分钟执行一次，到7点就不执行了。
        //后来周期设置成6点到11点，如果在这个期间所有用户都完成了，然后重启了任务，删除了所有key，但时间还是在6点到11点之间，任务还是会执行，redis中又没有已完成，所以还是会继续发送
        if (count == openIdList.size()){
            //所有用户都打卡了，重启一下任务，今天不发了，下一天再发
            String s = HttpUtil.get("http://localhost/testStopTask/task1");//停止任务
            String s1 = HttpUtil.get("http://localhost/testStarkTask");//启动任务
            //删除redis中当天的所有用户
            Boolean delete = redisTemplate.delete(key);
        }
*/
        JSONObject result = JSONUtil.parseObj(resp);
    }

    /**
     * 获取accessToken
     * @return
     * @throws IOException
     */
    public String getAccessToken() throws IOException {
        //先从redis中获取，如果没有说明过期了，重新请求微信服务器
        String access_token;
        access_token = (String) redisTemplate.opsForValue().get("access_token");
        log.info("从redis获取assess_token:{}",access_token);
        if (StringUtils.isEmpty(access_token))
        {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost(getAccessTokenUrl);
            HttpResponse execute = httpClient.execute(httpPost);
            InputStream content = execute.getEntity().getContent();
            OutputStream outputStream = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int n ;
            while ((n = content.read(bytes)) != -1) {
                outputStream.write(bytes,0,n);
            }
            outputStream.flush();
            outputStream.close();
            String string = outputStream.toString();
            JSONObject jsonObject = JSONUtil.parseObj(string);
            access_token = jsonObject.getStr("access_token");
            //将accessToken存入redis，并设置1小时55分钟的过期时间，因为accessToken的有效期为2小时，需要在过期前去换回新的token
            redisTemplate.opsForValue().set("access_token",access_token,115, TimeUnit.MINUTES);
            log.info("新accessToken【{}】存入redis",access_token);
        }
        return access_token;
    }

    /**
     * 获取用户列表
     * @param accessToken
     * @return
     */

    public JSONObject getUserList(String accessToken) {
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + accessToken + "&next_openid=";
        String resp = HttpUtil.get(requestUrl);
        JSONObject result = JSONUtil.parseObj(resp);
        System.out.println("用户列表:" + resp);
        return result;
    }
}

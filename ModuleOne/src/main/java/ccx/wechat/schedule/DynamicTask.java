package ccx.wechat.schedule;

import ccx.wechat.config.ScheduleConfig;
import ccx.wechat.config.WXConfig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledFuture;

@Component
public class DynamicTask{

    @Autowired
    ThreadPoolTaskScheduler scheduler;

    @Autowired
    private WXConfig wxConfig;

    @Autowired
    private RedisTemplate redisTemplate;

    public void startTask(){
        /**
         * corn表达式：秒 分 时 日期 月份 星期
         * 工作日的下午6点开始执行，每5分钟执行一次   0 0/5 18 ? * MON-FRI
         */
//        SendMsgTask task1 = new SendMsgTask("task1", "0/2 * * * * ?",wxConfig.getGetAccessTokenUrl(), wxConfig.getTemplateID());
        //这个corn表达式小时处只写18那么只会在18点的第0分钟开始每隔5分钟执行一次，到19点就不执行了。所以要改成18-23
        SendMsgTask task1 = new SendMsgTask("task1", "0 0/5 18-23 ? * MON-FRI",wxConfig.getGetAccessTokenUrl(), wxConfig.getTemplateID(), redisTemplate);
//        CustomizeTask task1 = new CustomizeTask("task1", "0/2 * * * * ?");
        ScheduledFuture<?> schedule = scheduler.schedule(task1, new CronTrigger(task1.getCorn()));
        ScheduleConfig.cacheSchedule.put(task1.getName(), schedule);
    }

    public boolean stopTask(String taskName){
        boolean flag = true;
        if (ScheduleConfig.cacheSchedule.isEmpty()) return flag;
        ScheduledFuture taskName1 = ScheduleConfig.cacheSchedule.get(taskName);
        if (taskName1 == null) return flag;
        boolean cancel = taskName1.cancel(true);//停止任务
        if (cancel){
            //停止成功
            ScheduleConfig.cacheSchedule.remove(taskName);
            return flag;
        }
        flag = false;
        return flag;
    }

    //自定义任务
    @Getter
    @NoArgsConstructor
    private class CustomizeTask implements Runnable{

        @Autowired
        private WXConfig wxConfig;

        private String name;//任务名称
        private String corn;//触发条件


        CustomizeTask(String name, String corn){
            this.name = name;
            this.corn = corn;
        }

        @Override
        public void run() {
            System.out.println("当前任务名称:" + name);
            System.out.println(wxConfig.getGetAccessTokenUrl());
        }
    }
}
